from typing import Dict, Tuple
from d3m import container
from d3m.container import ndarray as d3m_ndarray, Dataset, List
from d3m.container import DataFrame
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from common_primitives.utils import combine_columns
from d3m.primitive_interfaces.transformer import TransformerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer
import os
import numpy as np  # type: ignore
import cv2  # type: ignore
import copy
import typing

Inputs = DataFrame
Outputs = DataFrame


class Hyperparams(hyperparams.Hyperparams):
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='new',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned?",
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include a primary index column if input data has it. Applicable only if \"return_result\" is set to \"new\".",
    )


class VideoReader(TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a list of file names to video files and returns a
    list of length N of Numpy arrays of shape:
        N x F x H x W x C
    where N is the number of videos, F is the number of frames, C is the number of channels in an video
    (e.g., C = 1 for grayscale, C = 3 for RGB), H is the height, and W is the
    width. All videos are expected to be of the same shape and channel
    characteristics.
    """
    __author__ = 'University of Michigan, Eric Hofesmann, Nathan Louis, Madan Ravi Ganesh'
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'a29b0080-aeff-407d-9edb-0aa3eefbde01',
         'version': '0.1.0',
         'name': 'Video reader',
         'keywords': ['video', 'avi', 'mp4'],
         'source': {
            'name': 'common-primitives',
            'contact': 'mailto:davjoh@umich.edu'
         },



         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'
                           .format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.VideoReader',
         'algorithm_types': ['FILE_MANIPULATION'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._return_result = self.hyperparams['return_result']
        self._add_index_columns = self.hyperparams['add_index_columns']

    def _load_video(self, path: str) -> np.array:
        """
        Loads a video from a given path

        Arguments:
            :path: Path to the video to load

        Returns:
            :vid: Video loaded from the specified location
        """

        cap = cv2.VideoCapture(path)
        frames = []

        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            else:
                frames.append(frame)

        cap.release()
        return np.array(frames)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:

        output_dataframe = copy.copy(inputs)
        inputs_metadata = inputs.metadata

        new_column_name = 'data'
        output_metadata = inputs_metadata.clear().update((metadata_module.ALL_ELEMENTS, metadata_module.ALL_ELEMENTS,),
                                                         {'structural_type': d3m_ndarray, 'semantic_types': ('http://schema.org/VideoObject',), 'name': new_column_name})

        output_videos = List()
        video_shape = None

        if inputs_metadata.query(())['semantic_types'][0] != 'http://schema.org/VideoObject':
            raise ValueError("Input DataFrame must have a sematic_type of VideoObject")

        for vid_ind in range(len(inputs['filename'])):
            vid_name = inputs['filename'][vid_ind]
            base_data_path = inputs.metadata.query([str(vid_ind), 0])['location_base_uris'][0]
            file_name = os.path.join(base_data_path, vid_name)

            vid = self._load_video(file_name)

            out_vid = []
            for frame in vid:
                out_vid.append(frame)

            out_vid = d3m_ndarray(out_vid)
            output_videos.append(out_vid)

        np_dataframe = DataFrame(data={new_column_name: output_videos})
        np_dataframe.metadata = output_metadata

        filename_col_index = np.where(inputs.keys() == 'filename')[0][0]

        output_dataframe = combine_columns(return_result=self._return_result, add_index_columns=self._add_index_columns,
                                           inputs=output_dataframe, column_indices=[filename_col_index], columns_list=[np_dataframe])
        return CallResult(output_dataframe)
