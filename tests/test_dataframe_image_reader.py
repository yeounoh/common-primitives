import unittest
import os

from d3m import container

from common_primitives import dataset_to_dataframe, dataframe_image_reader


class DataFrameImageReaderTestCase(unittest.TestCase):
    def test_basic(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        dataframe_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataframe_hyperparams_class.defaults().replace({'dataframe_resource': '0'}))
        dataframe = dataframe_primitive.produce(inputs=dataset).value

        image_hyperparams_class = dataframe_image_reader.DataFrameImageReaderPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        image_primitive = dataframe_image_reader.DataFrameImageReaderPrimitive(hyperparams=image_hyperparams_class.defaults())
        images = image_primitive.produce(inputs=dataframe).value

        self.assertEqual(images.shape[0], 5)
        self.assertEqual(images.iloc[0, 0].shape[1:], (150, 3))
        self.assertEqual(images.iloc[1, 0].shape[1:], (32, 3))
        self.assertEqual(images.iloc[2, 0].shape[1:], (32, 3))
        self.assertEqual(images.iloc[3, 0].shape[1:], (28,))
        self.assertEqual(images.iloc[4, 0].shape[1:], (28,))

        self._test_metadata(images.metadata)

    def test_can_accept(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset_metadata = dataset.metadata.set_for_value(None)

        dataframe_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        dataframe_metadata = dataset_to_dataframe.DatasetToDataFramePrimitive.can_accept(method_name='produce', arguments={'inputs': dataset_metadata}, hyperparams=dataframe_hyperparams_class.defaults().replace({'dataframe_resource': '0'}))

        image_hyperparams_class = dataframe_image_reader.DataFrameImageReaderPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        images_metadata = dataframe_image_reader.DataFrameImageReaderPrimitive.can_accept(method_name='produce', arguments={'inputs': dataframe_metadata}, hyperparams=image_hyperparams_class.defaults())

        self._test_metadata(images_metadata)

    def _test_metadata(self, metadata):
        self.assertEqual(metadata.query_column(0)['structural_type'], container.ndarray)


if __name__ == '__main__':
    unittest.main()
