import unittest
import os
import numpy as np

from common_primitives.video_reader import VideoReader, Hyperparams
from d3m.container.dataset import D3MDatasetLoader
from common_primitives import dataset_to_dataframe
from d3m import container


class VideoReaderTestCase(unittest.TestCase):
    def test_produce(self):
        vr = VideoReader(hyperparams=Hyperparams.defaults())
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'video_dataset_1', 'datasetDoc.json'))
        
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))
        
        df0 = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=dataset_to_dataframe.Hyperparams(dataframe_resource='0')).produce(inputs=dataset).value
        
        data_run = vr.produce(inputs=df0).value
        
        data_run = np.array(data_run)
        assert((data_run.shape[0] == 2) and (data_run[0][0].shape[1:] == (240,320,3)) and (data_run[1][0].shape[1:] == (240,320,3)))

if __name__ == '__main__':
    unittest.main()
